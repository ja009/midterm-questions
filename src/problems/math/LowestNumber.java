package problems.math;

import databases.ConnectToSqlDB;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;



public class LowestNumber {

    public static Connection connect = null;
    public static Statement statement = null;
    public static PreparedStatement ps = null;
    public static ResultSet resultSet = null;

    public static void main(String[] args) throws Exception {
        /*
         * Write java solution to find the lowest number from this array. !
         * Use MySql Database to store and to retrieve.
         */
        int[] array = new int[]{211, 110, 99, 34, 67, 89, 67, 456, 321, 456, 78, 90, 45, 32, 56, 78, 90, 54, 32, 123, 67, 5, 679, 54, 32, 65};

        //find the lowest number from the array

        int size = array.length;
        System.out.println(size);

        //[5, 32, 32, 32, 34, 45, 54, 54, 56, 65, 67, 67, 67, 78, 78, 89, 90, 90, 99, 110, 123, 211, 321, 456, 456, 679]

        Arrays.sort(array);
        System.out.println("Sorted Array: " + Arrays.toString(array));
        int lowNum = array[0];
        System.out.println("Smallest element is: " + lowNum);


        ConnectToSqlDB connectToSqlDB = new ConnectToSqlDB();
        List<String> lowestValue = new ArrayList<String>();
        lowestValue.add(String.valueOf(lowNum));

//        try {
//            connectToSqlDB.insertDataFromArrayToSqlTable(array, "tbl_lowestNumber", "column_lowestNumber");
//            lowestValue = connectToSqlDB.readDataBase("tbl_lowestNumber", "column_lowestNumber");
//
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        System.out.println("Data is reading from the Table (tbl_primenumber) and displaying to the console");
//        for (String st : lowestValue) {
//            System.out.println(st);
//        }


        //connectToSqlDatabase();
        insertDataFromArrayListToSqlTable(lowestValue, "tbl_lowestNumber", "column_lowestNumber");
        readDataBase("tbl_lowestNumber","column_lowestNumber");
    }

    public static Properties loadProperties() throws IOException {
        Properties prop = new Properties();
        InputStream ism = new FileInputStream("src/problems/math/config.properties");
        prop.load(ism);
        ism.close();
        return prop;
    }

    public static Connection connectToSqlDatabase() throws IOException, SQLException, ClassNotFoundException {
        Properties prop = loadProperties();
        String driverClass = prop.getProperty("MYSQLJDBC.driver");
        String url = prop.getProperty("MYSQLJDBC.url");
        String userName = prop.getProperty("MYSQLJDBC.userName");
        String password = prop.getProperty("MYSQLJDBC.password");
        Class.forName(driverClass);
        connect = DriverManager.getConnection(url, userName, password);
        System.out.println("Database is connected");
        return connect;
    }


    public static void insertDataFromArrayListToSqlTable(List<String> list, String tableName, String columnName) {
        try {
            connectToSqlDatabase();

            for (String st : list) {
                ps = connect.prepareStatement("INSERT INTO " + tableName + " ( " + columnName + " ) VALUES(?)");
                ps.setObject(1, st);
                ps.executeUpdate();
            }

        } catch (IOException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    public static List<String> readDataBase(String tableName, String columnName) throws Exception {
        List<String> data = new ArrayList<String>();

        try {
            connectToSqlDatabase();
            statement = connect.createStatement();
            resultSet = statement.executeQuery("select * from " + tableName);
            data = getResultSetData(resultSet, columnName);
            System.out.println(data);
        } catch (ClassNotFoundException e) {
            throw e;
        } finally {
            close();
        }
        return data;
    }

    private static List<String> getResultSetData(ResultSet resultSet2, String columnName) throws SQLException {
        List<String> dataList = new ArrayList<String>();
        while (resultSet.next()) {
            String itemName = resultSet.getString(columnName);
            dataList.add(itemName);
        }
        return dataList;
    }

    private static void close() {
        try {
            if (resultSet != null) {
                resultSet.close();
            }
            if (statement != null) {
                statement.close();
            }
            if (connect != null) {
                connect.close();
            }
        } catch (Exception e) {

        }
    }
}


