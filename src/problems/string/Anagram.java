package problems.string;

import java.util.Arrays;
import java.util.Scanner;

public class Anagram {

        //Write a Java Program to check if the two String are Anagram. Two String are called Anagram when there is
        //same character but in different order.For example,"CAT" and "ACT", "ARMY" and "MARY".

        public static void main(String[] args) {
            Scanner scanner = new Scanner(System.in);
            System.out.println("write a word: ");
            String str1 = scanner.nextLine();
            System.out.println("write another word: ");
            String str2 = scanner.nextLine();

            str1 = str1.toLowerCase();
            str2 = str2.toLowerCase();

            // check if length is same
            if(str1.length() == str2.length()) {

                // convert strings to char array
                char[] charArray1 = str1.toCharArray();
                char[] charArray2 = str2.toCharArray();

                // sort the char array
                Arrays.sort(charArray1);
                Arrays.sort(charArray2);

                // if sorted char arrays are same
                // then the string is anagram
                boolean result = Arrays.equals(charArray1, charArray2);

                if(result) {
                    System.out.println(str1 + " and " + str2 + " are anagram.");
                }
                else {
                    System.out.println(str1 + " and " + str2 + " are not anagram.");
                }
            }
            else {
                System.out.println(str1 + " and " + str2 + " are not anagram.");
            }
        }

}
