package problems.string;

import java.util.Locale;

public class Permutation {

    public static void main(String[] args) {

        /*
         * Permutation of String "ABC" is "ABC" "BAC" "BCA" "ACB" "CAB" "CBA".
         * Write Java program to compute all Permutation of a String
         *
         */
        String rv20 = "Hello";
        rv20 = rv20.toLowerCase(Locale.ROOT); // to make sure theres no case sensitive exceptions
        printPermutation(rv20, "");
    }

    public static void printPermutation(String rv15, String rv16) {

        if (rv15.length() == 0) {       // If string is empty
            System.out.print(rv16 + " ");            // print rv16
            return;
        }

        boolean rv17[] = new boolean[26]; // Making a boolean array of size '26' cuz 26 alphabets
        // Its gon store false by default
        // If the alphabet is being used its gon say true
        for (int i = 0; i < rv15.length(); i++) {
            char ch = rv15.charAt(i);
            String ros = rv15.substring(0, i) + rv15.substring(i + 1); // complicated rambo jumbo

            if (rv17[ch - 'a'] == false) printPermutation(ros, rv16 + ch);
            rv17[ch - 'a'] = true;
        }
    }
}