package datastructure;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.sql.*;
import java.util.*;

public class UseMap {

    public static void main(String[] args) {
        /*
         * .

         *
         * Use MySql Database to store data and retrieve data.
         */
        //Map<String, List<string>> list = new HashMap<String, List<String>>();
        Map<String, List<String>> sports = new HashMap<String, List<String>>();
        List<String> topsoccerteams = new ArrayList<>();
        topsoccerteams.add("Real Madrid");
        topsoccerteams.add("Liverpool");
        topsoccerteams.add("Man United");
        topsoccerteams.add("Man City");

        List<String> bottomsoccerteam = new ArrayList<String>();
        bottomsoccerteam.add("Barcalona");
        bottomsoccerteam.add("PSG");
        bottomsoccerteam.add("Napoli");
        bottomsoccerteam.add("Arsenal");



        sports.put("List of Top-Teams:", topsoccerteams);
        sports.put("List of Bottom-Teams:", bottomsoccerteam);
        System.out.println(sports);
        System.out.println(sports.size());
        System.out.println(sports.get("List of Top-Teams:"));
        System.out.println(sports.get("List of Bottom-Teams:"));
        System.out.println(sports.get("List of Top-Teams:").get(0));
        System.out.println(sports.get("List of Bottom-Teams:").get(0));
        sports.remove("List of Top-Teams:",topsoccerteams.remove(3));
        sports.remove("List of Bottom-Teams:",bottomsoccerteam.remove(3));
        System.out.println(sports.get("List of Top-Teams:"));
        System.out.println(sports.get("List of Bottom-Teams:"));


        System.out.println("***********************************");
        System.out.println("Using for loop");


        for(int i = 0;i < 1;i++){

            System.out.println(sports);

        }

        System.out.println("***********************************");
        System.out.println("Using for each loop");
        System.out.println("Top Teams:\n");
        topsoccerteams.forEach(System.out::println);
        System.out.println("Bottom Teams:\n");
        bottomsoccerteam.forEach(System.out::println);


        System.out.println("***********************************");
        System.out.println("Using while loop");


        Iterator itr = sports.entrySet().iterator();

        while(itr.hasNext()){
            System.out.println(itr.next());
        }

        System.out.println("Inserting the data inside MySQL.");
        //firstPara("INSERT ");

    }

    public static String getPropertyFile(String filepath, String key) throws IOException {
        Properties properties = new Properties();
        properties.load(new FileInputStream(filepath));
        return properties.getProperty(key);
    }

    public static String getTextFile(String filepath) throws IOException {
        StringBuilder finalText = new StringBuilder();
        BufferedReader bufferedReader = new BufferedReader(new FileReader(filepath));
        String tempContainer = "";
        while ((tempContainer = bufferedReader.readLine()) != null) {
            finalText.append(tempContainer);
        }
        return finalText.toString();
    }

    public static int firstPara(String query) throws IOException, SQLException {
        String url = "jdbc:mysql://localhost:3306/classicmodels";
        String username = getPropertyFile("src/datastructure/config.properties", "username");
        String password = getPropertyFile("src/datastructure/config.properties", "password");
        Connection connection = DriverManager.getConnection(url, username, password);
        Statement statement = connection.createStatement();
        int resultSet = statement.executeUpdate(query);
        return resultSet;
    }

    public static ResultSet firstPara2(String query) throws IOException, SQLException {
        String url = "jdbc:mysql://localhost:3306/classicmodels";
        String username = getPropertyFile("src/datastructure/config.properties", "username");
        String password = getPropertyFile("src/datastructure/config.properties", "password");
        Connection connection = DriverManager.getConnection(url, username, password);
        Statement statement = connection.createStatement();
        ResultSet resultSet = statement.executeQuery(query);
        return resultSet;
    }

    public static void getValue(ResultSet resultSet, String columnName) throws SQLException {
        while (resultSet.next()) {
            System.out.println(resultSet.getString(columnName));
        }
    }
}
