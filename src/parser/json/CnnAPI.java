package parser.json;

import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class CnnAPI {
    /*
      You can get API_KEY from this below link. Once you have the API_KEY, you can fetch the top-headlines news.
      https://newsapi.org/s/cnn-api

      = 1c358a1edbb141928b3930ec5ad880f0

      Fetch This following CNN API, It will return some news in Json data. Store the data in the data.json file
      https://newsapi.org/v2/top-headlines?sources=cnn&apiKey=YOUR_API_KEY

	   Read the articles array and construct Headline news as

	   source,
	   author,
	   title,
	   description,
	   url,
	   urlToImage,
	   publishedAt
	   content.

	   You need to design News Data Model and construct headline news.
	   You can store in Map and then into ArrayList as your choice of Data Structure.

     */

    public static void main(String[] args) throws IOException, JSONException {
//        JSONObject rootObject = new JSONObject(new String(Files.readAllBytes(new File("src/json/parser/data.json").toPath())));
//        JSONArray array = rootObject.getJSONArray("articles");
//        List<Articles> list = new ArrayList<>();
//        for (int i = 0; i < array.length(); i++) {
//            JSONObject dataJson = array.getJSONObject(i);
//            ObjectMapper objectMapper = new ObjectMapper();
//            Articles data = null;
//            try {
//                data = objectMapper.readValue(dataJson.toString(), Articles.class);
//            } catch (JsonMappingException e) {
//                e.printStackTrace();
//            }
//            list.add(data);
//        }

        String output = getUrlContents("https://newsapi.org/v2/top-headlines?sources=cnn&apiKey=1c358a1edbb141928b3930ec5ad880f0");
        System.out.println(output);

    }
    private static String getUrlContents(String theUrl)
    {
        StringBuilder content = new StringBuilder();
        // Use try and catch to avoid the exceptions
        try
        {
            URL url = new URL(theUrl); // creating a url object
            URLConnection urlConnection = url.openConnection(); // creating a urlconnection object

            // wrapping the urlconnection in a bufferedreader
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
            String line;
            // reading from the urlconnection using the bufferedreader
            while ((line = bufferedReader.readLine()) != null)
            {
                content.append(line + "\n");
            }
            bufferedReader.close();
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
        return content.toString();
    }
}
